package singlejartest

import com.dukascopy.api.*
import com.dukascopy.api.feed.IFeedDescriptor
import com.dukascopy.api.feed.IFeedListener
import com.dukascopy.api.feed.util.RangeBarFeedDescriptor
import com.dukascopy.api.feed.util.TicksFeedDescriptor
import com.dukascopy.api.feed.util.TimePeriodAggregationFeedDescriptor
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*


class MultipleFeedsTest: IStrategy, IFeedListener {

    private var console: IConsole? = null
    private val LOGGER: Logger = LoggerFactory.getLogger(Test::class.java.simpleName)

    //on the left-hand side choosing particular IFeedDescriptor implementation
    // enforces the feed type - in this case - range bars
    @Configurable("range bar feed")
    var rangeBarFeedDescriptor = RangeBarFeedDescriptor(
            Instrument.EURUSD, PriceRange.TWO_PIPS, OfferSide.ASK)


    @Configurable("range bar feed")
    var tickFeedDescriptor = TicksFeedDescriptor(Instrument.EURUSD)

    @Configurable("any feed")
    var anyFeedDescriptor: IFeedDescriptor = TimePeriodAggregationFeedDescriptor(
            Instrument.EURUSD, Period.ONE_SEC, OfferSide.ASK, Filter.NO_FILTER)

    override fun onStart(context: IContext?) {
        LOGGER.info("Feed strategy started")

        console = context?.console
        context?.setSubscribedInstruments(HashSet(listOf(rangeBarFeedDescriptor.instrument, anyFeedDescriptor.instrument, tickFeedDescriptor.instrument)), true)
        context?.subscribeToFeed(rangeBarFeedDescriptor, this)
        context?.subscribeToFeed(anyFeedDescriptor, this)
        context?.subscribeToFeed(tickFeedDescriptor, this)
    }

    override fun onStop() {
        LOGGER.info("Feed strategy stopped")
    }

    /**
     * Not implemented
     */
    override fun onTick(instrument: Instrument?, tick: ITick?) {
        return
    }

    /**
     * Not implemented
     */
    override fun onBar(instrument: Instrument?, period: Period?, askBar: IBar?, bidBar: IBar?) {
        return
    }

    /**
     * Not implemented
     */
    override fun onMessage(message: IMessage?) {
        return
    }

    /**
     * Not implemented
     */
    override fun onAccount(account: IAccount?) {
        return
    }

    override fun onFeedData(feedDescriptor: IFeedDescriptor?, feedData: ITimedData?) {
        feedDescriptor?.let {
            if(it == this.rangeBarFeedDescriptor) {
                console?.out?.format("RANGE BAR completed of the feed: %s descriptor: %s",
                        feedData, feedDescriptor)?.println()
            } else {
                console?.info?.format("%s element completed of the feed: %s descriptor: %s",
                        feedDescriptor.dataType, feedData, feedDescriptor)?.println()
            }
        }
    }


}