package singlejartest;

import com.dukascopy.api.*;
import com.dukascopy.api.feed.ITickFeedListener;

import java.util.Collections;

public class Strategy1 implements IStrategy, ITickFeedListener {
    private IContext context;
    private IConsole console;

    public void onStart(IContext context) throws JFException {
        this.context = context;
        console = context.getConsole();
        context.setSubscribedInstruments(Collections.singleton(Instrument.EURUSD));
        context.subscribeToTicksFeed(Instrument.EURUSD, this);
    }

    public void onAccount(IAccount account) throws JFException {
    }

    public void onMessage(IMessage message) throws JFException {
    }

    public void onStop() throws JFException {
    }

    public void onTick(Instrument instrument, ITick tick) {
            console.getErr().println("onTick");
    }

    public void onBar(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {
        console.getErr().println("onBar, period: "+period.toString());
    }
}