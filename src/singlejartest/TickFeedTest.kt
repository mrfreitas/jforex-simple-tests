package singlejartest

import com.dukascopy.api.*
import com.dukascopy.api.feed.IFeedDescriptor
import com.dukascopy.api.feed.IFeedListener
import com.dukascopy.api.feed.util.TickBarFeedDescriptor
import com.dukascopy.api.feed.util.TicksFeedDescriptor
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class TickFeedTest: IStrategy, IFeedListener {

    private var console: IConsole? = null
    private val LOGGER: Logger = LoggerFactory.getLogger(Test::class.java.simpleName)

    @Configurable("tick feed")
    var tickFeedDescriptor =  TickBarFeedDescriptor(Instrument.EURUSD,
            TickBarSize.valueOf(5),
            OfferSide.ASK
    )

    override fun onStart(context: IContext?) {
        LOGGER.info("Tick feed strategy started")

        tickFeedDescriptor.offerSide = OfferSide.ASK
        console = context?.console
        context?.subscribedInstruments = Collections.singleton(tickFeedDescriptor.instrument)
        context?.subscribeToFeed(tickFeedDescriptor, this)
    }

    override fun onStop() {
        LOGGER.info("Tick feed strategy stopped")
    }

    /**
     * Not implemented
     */
    override fun onTick(instrument: Instrument?, tick: ITick?) {
        LOGGER.info("onTick")
    }

    /**
     * Not implemented
     */
    override fun onBar(instrument: Instrument?, period: Period?, askBar: IBar?, bidBar: IBar?) {
        LOGGER.info("onBar")
        console?.info?.format("Instrument: %s ", instrument)?.println()
        console?.info?.format("Period: %s ", period)?.println()
        console?.info?.format("AskBar: %s ", askBar)?.println()
        console?.info?.format("bidBar: %s ", bidBar)?.println()
    }

    /**
     * Not implemented
     */
    override fun onMessage(message: IMessage?) {
        LOGGER.info("onMessage")
    }

    /**
     * Not implemented
     */
    override fun onAccount(account: IAccount?) {
        LOGGER.info("onAccount")
    }

    override fun onFeedData(feedDescriptor: IFeedDescriptor?, feedData: ITimedData?) {
        LOGGER.info("onFeedData")
        console?.info?.format("%s element completed of the feed: %s descriptor: %s",
                feedDescriptor?.dataType, feedData, feedDescriptor)?.println()
    }


}