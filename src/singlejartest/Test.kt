package singlejartest

import com.dukascopy.api.*
import com.dukascopy.api.Period.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class Test : IStrategy {
    private val LOGGER: Logger = LoggerFactory.getLogger(Test::class.java.simpleName)
    private val periods = arrayOf(
            TEN_SECS, ONE_MIN, FIVE_MINS, TEN_MINS, FIFTEEN_MINS, THIRTY_MINS, ONE_HOUR, FOUR_HOURS, DAILY, WEEKLY, MONTHLY
    )

    override fun onStart(context: IContext?) {
        LOGGER.info("Super strategy started")

        val console = context?.console
        val history = context?.history

        val lastTickTime: Long = history?.getLastTick(Instrument.EURUSD)?.time ?: 0
        val lastBarTime: Long = history?.getBarStart(ONE_MIN, lastTickTime) ?: 0
        var bars: List<IBar>? = null
        history?.let {
            bars = it.getBars(Instrument.EURUSD, ONE_MIN, OfferSide.BID, Filter.NO_FILTER, 10, lastBarTime, 0)
        }


        var maxHigh = Double.MIN_VALUE
        bars?.let {
            for (bar in it) {
                if (maxHigh < bar.high) {
                    maxHigh = bar.high
                }
                console?.out?.println(bar)
            }
            console?.out?.println("bars: " + (it.size) + " high=" + maxHigh)
        }

    }

    override fun onStop() {
        LOGGER.info("Super strategy stoped")
    }

    override fun onTick(instrument: Instrument?, tick: ITick?) {
        LOGGER.info("onTick: $instrument")
    }

    override fun onBar(instrument: Instrument?, period: Period?, askBar: IBar?, bidBar: IBar?) {

    }

    override fun onMessage(message: IMessage?) {
    }

    override fun onAccount(account: IAccount?) {
    }
}